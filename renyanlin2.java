package com.yjkh.friend;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yjkh.login.Ui_Login;
import com.yjkh.main.R;
import com.yjkh.main.Ui_Home;
import com.yjkh.main.Ui_main;
import com.yjkh.service.ProductInfo;
import com.yjkh.service.SoapServiceFriends;
import com.yjkh.service.getFriendWebService;
import com.yjkh.utils.Constants;
import com.yjkh.utils.CustomProgressDialog;

public class Ui_AddFriend extends Activity implements OnClickListener {
	TextView MoneyTv, PointTv;
	TextView FriNameTv, FriNickTv, FriSexTv, FriAreaTv, FriSignTv, FriHobbyTv;
	TextView FriAddTitle;
	Button FriBtn;
	ImageView FriendBtn_Back,FriendHead;
	int uname;
	CustomProgressDialog pd;
	SharedPreferences sp;
	int did;
	Bitmap DownBitmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ui_addfriend);
		initView();
		initData();
	}

	public void initView() {
		MoneyTv = (TextView) findViewById(R.id.MoneyTv);
		PointTv = (TextView) findViewById(R.id.PointTv);
		FriNameTv = (TextView) findViewById(R.id.FriNameTv);
		FriNickTv = (TextView) findViewById(R.id.FriNickTv);
		FriSexTv = (TextView) findViewById(R.id.FriSexTv);
		FriAreaTv = (TextView) findViewById(R.id.FriAreaTv);
		FriSignTv = (TextView) findViewById(R.id.FriSignTv);
		FriHobbyTv = (TextView) findViewById(R.id.FriHobbyTv);
		FriAddTitle = (TextView)findViewById(R.id.FriAddTitle);
		FriendHead = (ImageView)findViewById(R.id.FriendHead);
		FriBtn = (Button) findViewById(R.id.FriBtn);
		FriendBtn_Back = (ImageView) findViewById(R.id.FriendBtn_Back);
		sp = getSharedPreferences(Constants.USERSHARED, MODE_PRIVATE);
		FriBtn.setOnClickListener(this);
		FriendBtn_Back.setOnClickListener(this);
		uname = getIntent().getIntExtra("uname", 0);
		if ("Ui_AddFriend".equals(getIntent().getStringExtra("activity"))) {
			FriBtn.setVisibility(View.GONE);
			FriAddTitle.setText("好友信息");
		}
		pd = CustomProgressDialog.createDialog(this);
	}

	public void initData() {
		/*String url = "appmember!getByUname.action";
		ProductInfo product = new ProductInfo(this, handler, url);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("uname", uname));
		product.FindFriend(params);*/
		new Thread(new Runnable() {
			public void run() {
				getFriendWebService.callService(uname, handler);
			}
		}).start();
	}

	public void addFri() {
//		pd.setTitle("正在添加好友..");
		new Thread(new Runnable() {
			public void run() {
				SoapServiceFriends.callService(sp.getInt("id", 0), did, "addFriend", handler);
			}
		}).start();
	}

	public Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constants.HANDLER_WHAT_START:
				// 开始
				pd.show();
				break;
			case Constants.HANDLER_WHAT_EXCEPTION:
				// 链接服务器失败
				Toast.makeText(Ui_AddFriend.this, "连接服务器异常", 1).show();
				pd.dismiss();
				break;
			case Constants.HANDLER_WHAT_NULL:
				// 没有此用户信息
				pd.dismiss();
				Toast.makeText(Ui_AddFriend.this, "没有此用户信息！", 1).show();
				break;
			case 2:
				// 获取用户信息异常
				pd.dismiss();
				Toast.makeText(Ui_AddFriend.this, "获取用户信息异常！", 1).show();
				break;
			case 1000:
				// 得到用户信息
				pd.dismiss();
				String result = msg.obj.toString();
				System.out.println(result);
				getJsonInfo(result);
				break;
			case 101:
				// 添加成功
				pd.dismiss();
				getJsonAddFriend(msg.obj.toString());
				break;
			case 1001:
				//设置好友头像
				FriendHead.setImageBitmap(DownBitmap);
				break;
			default:
				break;
			}
		};
	};
	public void getJsonAddFriend(String json){
		try {
			JSONObject obj = new JSONObject(json);
			Iterator<String> it = obj.keys();
			while (it.hasNext()) {
				String str = it.next();
				if (str.equals("error")) {
					Toast.makeText(Ui_AddFriend.this, "好友已存在", 0).show();
				} else if (str.equals("success")) {
					Toast.makeText(Ui_AddFriend.this, "添加成功", 0).show();
					Constants.addfriend = 1;
					finish();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getJsonInfo(String result) {
		try {
			JSONObject object = new JSONObject(result);
			did = object.getInt("id");
			MoneyTv.setText("虚拟财富：" + object.getString("property"));
			PointTv.setText("火力值：" + object.getString("point"));
			FriNameTv.setText("姓名：" + object.getString("trueName"));
			FriNickTv.setText("昵称：" + object.getString("nickname"));
			if (object.getString("sex").equals("1")) {
				FriSexTv.setText("性别：" + "男");
			} else if (object.getString("sex").equals("0")) {
				FriSexTv.setText("性别：" + "女");
			} else {
				FriSexTv.setText("性别：" + "");
			}
			FriAreaTv.setText("地区：" + object.getString("corporation"));
			// 签名和兴趣爱好没有
			FriSignTv.setText("签名：");
			FriHobbyTv.setText("兴趣爱好：");
			downImage(Constants.WEBSERVICEIMAGE+object.getString("facePath"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.FriendBtn_Back:
			finish();
			break;
		case R.id.FriBtn:
			// 添加好友
			if (sp.getBoolean("Login", false)) {
			} else {
				showDialog();
				return;
			}
			addFri();
			break;

		default:
			break;
		}
	}
	
	public void showDialog() {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(Ui_AddFriend.this);
		alertDialog.setIcon(android.R.drawable.ic_dialog_info);
		alertDialog.setTitle(Ui_AddFriend.this.getString(R.string.app_tishi))
				.setMessage("亲，您还没有登录哦~");
		alertDialog.setPositiveButton(Ui_AddFriend.this.getString(R.string.app_login),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Intent i = new Intent(Ui_AddFriend.this,Ui_Login.class); 
						i.putExtra("login", 1);
						Ui_AddFriend.this.startActivity(i);
					}
				});
		alertDialog.setNegativeButton(
				Ui_AddFriend.this.getString(R.string.app_quxiao),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
			}
		});
		alertDialog.show();
	}
	public void downImage(final String path) {
		new Thread(new Runnable() {
			public void run() {
				try {
					URL url = new URL(path);
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.setConnectTimeout(5 * 1000);
					InputStream inStream = conn.getInputStream();
					ByteArrayOutputStream outStream = new ByteArrayOutputStream();
					byte[] buffer = new byte[1204];
					int len = 0;
					while ((len = inStream.read(buffer)) != -1) {
						outStream.write(buffer, 0, len);
					}
					inStream.close();
					outStream.close();
					byte[] data = outStream.toByteArray();
					DownBitmap = BitmapFactory.decodeByteArray(data, 0,
							data.length);
					handler.sendEmptyMessage(1001);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}
